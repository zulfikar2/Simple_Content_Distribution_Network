# Simple Content Distribution Network

This is Simple Content Distribution Application
We are going to implement following components:
Application client at user’s side.
Dummy web server www.hiscinema.com
Dummy local DNS server.
Dummy authoritative DNS server for domain hiscinema.com which will return URL for herCDN.com
Dummy authoritative DNS server for domain herCDN.com which will return IP address for www.herCDN.com
Dummy content server www.herCDN.com which delivers content to our client  using HTTP. 

Read ReadMe.doc and System.doc for more information
