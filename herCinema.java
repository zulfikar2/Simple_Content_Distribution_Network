import java.io.*;
import java.util.*;
import java.net.*;

class herCinema {
  
  public final static String IP = "localhost";
  //actual video
  public static String video="";
  
  
  public final static int portNum = 40504;
  
  public static void main(String args[]) throws IOException, ClassNotFoundException {
   
    while (true) {
      System.out.println("Starting herCinema.com...");
      //dont worry about this
      System.out.println("My IP is : " + InetAddress.getLocalHost().getHostAddress() + ":" + portNum);
      ServerSocket herCinema = null;
      Socket connectionSocket = null;
      BufferedOutputStream outToClient = null;
      InputStream inStream = null;
      OutputStream outStream = null;
    
      
      int statusCode = 0;
      
      String videoURL= "";
      
      System.out.println("Waiting for Client request...");
      herCinema = new ServerSocket(portNum);
    
      while(true) {
        try {
          connectionSocket = herCinema.accept();
          inStream = connectionSocket.getInputStream(); 
          outToClient = new BufferedOutputStream(connectionSocket.getOutputStream());
        } catch (IOException ex) {
          System.out.println(ex);
        }
        try{
          ObjectInputStream ois = new ObjectInputStream(inStream);
          HTTP_Request request = (HTTP_Request) ois.readObject();
          System.out.println(request.getURL());
          
          if (request.getVersion() != 1.1) {
            statusCode = 505;
          }
          
          if (request != null) {
            if (request.getMethod().equals("GET")) {
              
       if(request.getURL().equals("Short.mp4")){
          video="Short.mp4";
        }
       else if(request.getURL().equals("Short1.mp4")){
          video="Short1.mp4" ;
        }
        else if(request.getURL().equals("Short2.mp4")){
          video="Short2.mp4" ;
        }
        else if(request.getURL().equals("Short3.mp4")){
          video="Short3.mp4" ;
        }
       else  if(request.getURL().equals("Short4.mp4")){
          video="Short4.mp4" ;
        }
       else  if(request.getURL().equals("Short5.mp4")){
          video="Short5.mp4" ;
        }
        
        
              System.out.println("Obtained GET request from client for : " + request.getURL());
              if(!request.getURL().equalsIgnoreCase(video))
                statusCode = 404;
              else
                statusCode = 200;
            }
            else
              statusCode = 400;
          }
        } catch (IOException e) {
          System.out.println(e);
        }
       
        outStream = connectionSocket.getOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream(outStream);
        HTTP_Response response = new HTTP_Response(1.1, statusCode);
        oos.writeObject(response);
        System.out.println("Sending HTTP response to GET request...");
        
        //if get is good n stuff, send file
        // 
        /*
        if(request.getURL() == "Short.mp4"){
          video="Short.mp4";
        }
       else if(request.getURL() == "Short1.mp4"){
          video="Short1.mp4" ;
        }
        else if(request.getURL() == "Short2.mp4"){
          video="Short2.mp4" ;
        }
        else if(request.getURL() == "Short3.mp4"){
          video="Short3.mp4" ;
        }
       else  if(request.getURL() == "Short4.mp4"){
          video="Short4.mp4" ;
        }
       else  if(request.getURL() == "Short5.mp4"){
          video="Short5.mp4" ;
       }
       */
        if(statusCode == 200) {
          InetAddress IPAddress = connectionSocket.getInetAddress();
          int port = connectionSocket.getPort();
         
          if (outToClient != null) {
            
            File myFile = new File(video);
            byte[] mybytearray = new byte[(int) myFile.length()];
            
            FileInputStream fis = null;
            
            try {
              fis = new FileInputStream(myFile);
            } catch (FileNotFoundException ex) {
              System.out.println(ex);
            }
            BufferedInputStream bis = new BufferedInputStream(fis);
            
            try {
              bis.read(mybytearray, 0, mybytearray.length);
              outToClient.write(mybytearray, 0, mybytearray.length);
              outToClient.flush();
              outToClient.close();
              
              System.out.println(video + " was sent");
            } catch (IOException ex) {
              System.out.println(ex);
            }
          }
        }
      }
    }
  }
}
